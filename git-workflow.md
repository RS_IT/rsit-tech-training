**Table of Contents**  *generated with [DocToc](http://doctoc.herokuapp.com/)*

- [Git Workflow](#markdown-header-git-workflow)
	- [Branches](#markdown-header-branches)
		- [Master Branch](#markdown-header-master-branch)
		- [Develop Branch](#markdown-header-develop-branch)
	- [Tags](#markdown-header-tags)
	- [Typical Work Flow](#markdown-header-typical-work-flow)
	- [End To End Example](#markdown-header-end-to-end-example)

# Git Workflow

## Branches

We have 2 main branches in each repository - `develop` and `master`.

Except for trivial commits, all work should be done via branching. Only trivial commits and tags should occur directly on our main branches.

### Master Branch

`master` should be considered the production branch. When a release should happen, code should be merged from `develop` into `master` and a tag created for that release.

### Develop Branch

`develop` is the main branch for all work. When implementing a feature or bugfix, branch off of `develop` and merge back via pull request.

## Tags

A tag is an identifier for a particular commit. For our workflow, every release should be via a tag. We follow [semantic versioning](http://semver.org/) for our tags.  Tags and version numbers should always be numeric (X.Y.Z), never prefixed with 'v' or any other character.

## Typical Work Flow ##

Specific commands can be found in the examples below this section.

1. Create a new branch for your work.

If creating a branch using JIRA, the web interface will automatically specify the name according to these conventions.

 - You should pull the latest from `develop` and create your new branch at the latest commit in `develop`.
 - Branch names should be all lowercase, except for JIRA issue references, and use "-" for breaks.
 - If the task you're working on is logged in Jira (should be) then use the Jira ID as your branch name.  For example, if I'm working on AST-166 in Jira I would create a branch called `AST-166-task-description-here`.

2. More, Smaller Commits

 - All but the simplest tasks can be divided into smaller logical chunks.  Making each of these chunks a separate commit (with meaningful commit comments) will make code reviews easier and make bugs easier to trace.
 - Commit often, even if the commits are just 'wip' commits. Commits can be squashed as needed before a pull request is issued.

3. For long running tasks, merge `develop` into your branch often

 - Obviously if `develop` doesn't have any new commits you can skip this step.
 - Do this at least once per day to head off potential merge conflicts before they become unmanageable.
 - Do this before you do a pull request.

4. Issue a Pull Request

When the work in your branch is done - or at least stable - you can initiate a pull request which will prompt someone other than yourself to do a code review and ultimately merge your changes into develop.

 - It is **important** that you make sure you do a pull request against a base of `develop`, with your branch as the `head` branch.
 - Create the pull request, with comments that show the bugfix or feature addition, or else detail the reason for the change.
 - Notify the person who will review the code of the request. They can then make comments at specific points in the code via Bitbucket.
 - As long as a pull request is open, additional commits will automatically show up in the PR.
 - Make sure to check the box that deletes the branch when merged.

5. Rework

 - Often the person reviewing your code will find issues.  After receiving their feedback you will need to address those issues and check in new code changes to your existing branch.  Then notify the original reviewer that your changes are in so they can resume the review.
 - This step may need to be repeated multiple times.
 - Keep in mind that every issue found in review is an issue you didn't introduce into the code, so it's a good thing.

7. Reviewer approves and merges the PR into develop

- You will need to delete your local branch manually.

## End To End Example

Below is a complete command flow for a dev who wants to:
- Get the code
- (some time later, pull down any changes that have happened)
- make a new branch for a new feature
- make some changes
- commit
- pull in any new changes to develop that have happened while feature was worked on
- push changes
- issue PR
- delete local branch once PR is closed
- get latest code
- assuming this is in a terminal/DOS prompt, and that the user is in the correct folder that's already been created

````bash
    # get code
    git clone git@bitbucket.org:ebus/as-dev-frontend.git

	# fetch is 'get but do not merge stuff'
	git fetch

    # checkout is 'get some particular branch, tag, commit, etc'
	# ex branch
    git checkout develop

    # pull is 'get and merge stuff'
	git pull

    # make a new branch that tracks origin/develop
    # this means when you run `git pull`, it will pull and merge from origin/develop
	git checkout -b my-cool-feature origin/develop

    # put your branch on Bitbucket as well as local
    git push origin my-cool-feature

	# do work here, then add all changed and new files to commit
	git add .

	# will open your editor for a commit message
	git commit

    # push the changes you just committed
	git push origin my-cool-feature

    # when ready for a PR
	# pull in any develop changes that happened
	# assumes still on the same my-cool-feature branch
	git pull

    # assuming the PR was merged and the remote branch deleted
	# you can clean up your local branches that have been merged into develop if you are all done.
	git branch -D my-cool-feature

	# to get rid of local tracking branches that do not exist anymore (deleted on the server) This will not effect your local work.
	git remote prune origin

    # get latest code, ready to repeat again
    git checkout develop
    git pull
````
