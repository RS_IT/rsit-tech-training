# SSH/Bash/Git Setup

## Windows Setup

* If you don't already have them installed, install Git, SourceTree and Notepad++ (or your preferred editor) from the [the official OA software site](http://www.software.oneamerica.com). Search for them by name. Make sure to click install, then click the shortcuts in your Start menu to complete the installation. See below for more instructions on installing Git.
* You must set windows to show hidden files/folders.
    * Open Windows Explorer, then go to 'Tools' in the menu (hit `alt-t` if not already showing, or click 'Organize' and under 'Layout', choose 'Menu Bar')
    * Go to `Folder Options`
    * Go to `View`
    * Uncheck `Hide Extensions for known file types`
    * Uncheck `Hide protected operating system files`
    * Select `Show hidden files, folders, and drives`

### Windows SourceTree Setup
*NOTE - Those of you more familiar with UNIX command line and plan to use just the Git BASH shell can skip the installation of SourceTree.

SourceTree is an officially approved git GUI - download from [the OA software installation site](http://www.software.oneamerica.com). Search for 'source'.

Once you have installed SourceTree AND finished this document/setup, you should view the document [on SourceTree setup](https://bitbucket.org/ebus/tech-training/src/develop/git-ui-clients.md) and follow the instructions under `Setup for using SourceTree via SSH`. You may skip the first part.

### Windows Git-Bash setup

The first step in this process can be scripted. Right click the link, choose 'Save As', and save to your desktop.

*NOTE - These scripts should create the files you need automatically in a `C:\DEV` folder.  It is recommended that you use the local hard drive to store your repositories. LAN drives will cause slow performance with Git. You should edit the scripts to point to your preferred location before running. Only run these if this is a new setup, or you are willing to blow away your setup and recreate it. If you run into any problems with those scripts, follow the manual steps below.*

* [Batch file for environment variable](https://bitbucket.org/ebus/tech-training/raw/develop/oa-home-setup.rename-to-dot-bat.txt)

Once saved, rename the file to have a `.bat` extension. You can do this by clicking the file once, then hitting `F2`, or by right-clicking and choosing `Rename`. You may get a warning about 'If you change a file name....' - you can click 'Yes' if you are asked 'Are you sure....'. Then double-click that file to run it. It will briefly open and then close a command prompt.

Make sure you have installed Git for Windows from [the official OA software site](http://www.software.oneamerica.com). Search for 'git' there. There should be a single result for a software version of 1.9.0.

* Choose the Git-Bash command prompt option when installing
* You **MUST** run the program to complete installation. Once it runs successfully, you may close it.
* IMPORTANT - this installer only works if you intend to install to the C drive. If you will be installing to another location, you must download and install it manually.

Right click the link below, choose 'Save As', and save to your desktop with the `.sh` extension.

- [Shell script for git/bash/ssh setup](https://bitbucket.org/ebus/tech-training/raw/develop/oa-setup.sh)

Remember to edit the script and change `C:\DEV` to whatever your preference may be on the C:\ drive if you wish.  From here on out in the instructions I will refer to C:\DEV.
Once saved, double-click that file to run.  It will briefly open and then close a command prompt.

**If it doesn't run automatically**, then open git-bash if not already open, and type the following command, replacing USERNAME with your windows login ID, then hit enter: `/c/Users/USERNAME/Desktop/os-setup.sh`

Next, open the git-bash shortcut in your start menu. Right click the Git Bash icon on your taskbar and pin it. Right click the icon again and choose properties. It is important to click the properties on the pinned icon because you cannot edit the properties of the icon in the Start menu without administrator privaledges.

* Set to start in `C:\Dev` or your custom location on the C:\ drive.  By typing `%HOME%` in the "start in" path you can accomplish the same thing.
* Set to run maximized as well for easier visibility.
* Now open git-bash. Verify the directory it starts in by typing `pwd` - it should return `/c/dev/`
    * git-bash is a unix environment, so it expects forward slashes to separate directories. You cannot use windows style paths like `C:\DEV`. You should use `/c/dev/git` instead, for example.

Next, follow step 6 from [Atlassian Docs for SSH Setup on Windows](https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git). This is the section labeled `Step 6. Install the public key on your Bitbucket account`. You will have to use Notepad ++ to open the `C:\DEV\.ssh\id_rsa.pub` file, and copy the contents to the clipboard.

Once you add that to Bitbucket, close and reopen git-bash. You should see a message about 'Identity added: /c/DEV/.ssh/is_rsa'.

Now run this command: `ssh -T git@bitbucket.org`. You should get a prompt about `RSA key fingerprint...` and `Are you sure you want to continue connecting`. Answer `yes`. Then you should see a short delay, then a message saying `You can use git or hg to connect to Bitbucket. Shell access is disabled`. This means you have authenticated to Bitbucket.

Finally, make sure you setup your username and email address for git.  You can run this command in git-bash (replace the all CAPS items appropriately):

* `git config --global user.name 'FIRSTNAME LASTNAME'` (hit enter)
* `git config --global user.email MYADDRESS@oneamerica.com` (hit enter)

### Manual Setup (only needed if the scripts above fail)

* create a `git` folder in your C:\DEV folder: `C:\DEV\git`
    * that will be your `HOME` directory (`%HOME%` when you need to script it in Windows)
* Set a user environment variable.
    * If you don't have admin rights, you can do this by going to the `Start` button, then search for 'environment' and choose 'Edit environment variables for my account'.
    * If you do have admin rights, you can do this: Right click 'My Computer', Properties, Advanced, Environment Variables
    * Click 'New' in the section for 'User Variables' (the top section of the dialog)
    * Create a variable named `HOME` and set it to `C:\DEV\git` It should be a User variable.
    * If you already have a folder containing git repos, you can move it w/o issue into the `C:\DEV\git` folder.
    * Using a `git` folder allows easier scripting across machines/environments.
    * All repos should be cloned inside that directory.

#### Config Files

There is a `configfiles` directory in this repository that contains several files already setup for your convenience. Copy them into the `C:\DEV\git` folder on your machine as is, keeping any nested folders, then verify that the contents match what's expected based on the instructions below.

#### SSH setup

* Use Notepad++ for all of the steps below - using Wordpad or Notepad will result in incorrect formatting and will not work. You can install Notepad++ from the [officel OA software site](https://www.software.oneamerica.com).
* The `.bashrc` and `.bash_profile` files mentioned in the URL below have already been created if you copied the contents of the `configFiles` directory to your local machine.
* Follow the steps at this URL:
    * [Atlassian Docs for SSH Setup on Windows](https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git)
    * You can start with the 3rd step: `Step 3. Set up your default identity`
    * At the step where the prompt asks you `Enter file in which to save the key`, make sure you change the location for the SSH file location to be `/c/dev/git/.ssh/id_rsa`
    * Passphrase is not needed, just hit enter twice when asked.
    * If you created a key with a passphrase, you can delete that key locally and from Bitbucket and create a new one w/o a passphase. Make sure you delete your existing key on Bitbucket, and add the new one.
* Verify you have a `C:\DEV\git\.ssh\config` file with the following content. If you copied the contents of the `configFiles` directory as detailed above, this file will be in place already.

        Host bitbucket.org
         IdentityFile ~/.ssh/id_rsa
         Port 443
         Hostname altssh.bitbucket.org

#### Git setup

* Verify you have a .gitconfig` file in `C:\DEV\git' (should have been copied as specified above).
* Make sure you setup your username and email address
    * You can run this command in git-bash: `git config --global user.name MY NAME` and `git config --global user.email my@email.address`
    * You can also do this by editing the `C:\DEV\git\.gitconfig` file and updating it with your username and email address.
    * You can also set that up by running SourceTree - it will read those values if present, or ask you to set them up if not.

## Advanced Setup

* If you are using a unix based desktop system, [SCM Breeze](https://github.com/ndbroadbent/scm_breeze) is a nice addition to your toolset versus using the above aliases.
* You might also look at this to explain the aliases: [http://haacked.com/archive/2014/07/28/github-flow-aliases/](http://haacked.com/archive/2014/07/28/github-flow-aliases/)
* If you want to set Notepad++ as your commit message editor, you can uncomment the line in `C:\yourpath\.gitconfig` that starts with `editor` (via [Stack Overflow](http://stackoverflow.com/a/2486342))
* To use [Beyond Compare](http://www.scootersoftware.com/features.php) as a difftool, see this link [http://www.scootersoftware.com/support.php?zz=kb_vcs#gitwindows](http://www.scootersoftware.com/support.php?zz=kb_vcs#gitwindows), or uncomment the `difftool` lines in `C:\DEV\.gitconfig`.

## Mac Setup
For mac, suggested path is [homebrew](http://brew.sh/).
Run the commands on the page above, then:

    brew install git