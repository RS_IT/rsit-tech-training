mkdir /c/DEV
mkdir /c/DEV/.ssh
mkdir /c/DEV/git

touch /c/DEV/.ssh/config
touch /c/DEV/.gitconfig
touch /c/DEV/.bash_profile
touch /c/DEV/.bashrc

read -d '' bashprofile <<"BLOCK"
if [ -f ~/.bashrc ]; then
   source ~/.bashrc
   fi
BLOCK

read -d '' bashrc <<"BLOCK"
SSH_ENV=$HOME/.ssh/environment

# start the ssh-agent
function start_agent {
    echo "Initializing new SSH agent..."
    # spawn ssh-agent
    /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
    echo succeeded
    chmod 600 "${SSH_ENV}"
    . "${SSH_ENV}" > /dev/null
    /usr/bin/ssh-add
}

if [ -f "${SSH_ENV}" ]; then
     . "${SSH_ENV}" > /dev/null
     ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
        start_agent;
    }
else
    start_agent;
fi

alias gs='git status'
alias ga='git add'
alias got='git'
alias get='git'
BLOCK

read -d '' gitconfig <<"BLOCK"
[user]
	name =
	email =
[core]
	autocrlf = true
	# verify path, uncomment to use Notepad++ for commit messages
	# editor = 'C:/Program Files (x86)/Notepad++/notepad++.exe' -multiInst -notabbar -nosession -noPlugin
[difftool "bc3"]
	# install Beyond Compare v4 and uncomment line below (verify path)
	# path = "C:/Program Files (x86)/Beyond Compare 4/BComp.exe"
[diff]
	# install Beyond Compare v4 and uncomment line below
	# tool = bc3
[alias]
	ci = commit
	st = status
	br = branch
	co = checkout
	ec = config --global -e
	up = !git pull --rebase --prune $@ && git submodule update --init --recursive
	cob = checkout -b
	cm = !git add -A && git commit -m
	save = !git add -A && git commit -m 'SAVEPOINT'
	wip = !git add -u && git commit -m "WIP"
	undo = reset HEAD~1 --mixed
	amend = commit -a --amend
	wipe = !git add -A && git commit -qm 'WIPE SAVEPOINT' && git reset HEAD~1 --hard
	bclean = "!f() { git branch --merged ${1-master} | grep -v " ${1-master}$" | xargs -r git branch -d; }; f"
	bdone = "!f() { git checkout ${1-master} && git up && git bclean ${1-master}; }; f"
[push]
	default = matching
BLOCK

read -d '' sshconfig <<"BLOCK"
Host bitbucket.org
 IdentityFile ~/.ssh/id_rsa
 Port 443
 Hostname altssh.bitbucket.org
BLOCK

echo "$bashprofile" >> /c/DEV/.bash_profile
echo "$bashrc" >> /c/DEV/.bashrc
echo "$gitconfig" >> /c/DEV/.gitconfig
echo "$sshconfig" >> /c/DEV/.ssh/config

ssh-keygen -t rsa -f /c/DEV/.ssh/id_rsa -P ''
