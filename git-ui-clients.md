# Git UI Clients

## SouceTree

SourceTree is an officially approved git GUI - download from [the OA software installation site](http://www.software.oneamerica.com). Search for 'source'.

### Setup for using SourceTree via HTTPS

On the initial launch screen, enter your full name and OneAmerica email address.

Select the 'Allow Sourcetree to modify your global config files' option.

Also select the 'automatic line endings' option.

Choose 'No' when prompted about the option for a global .gitgnore file.

If prompted to download/install Mercurial, select "I don't want to use mmercurial".

Select `Use OpenSSH`, although you will not do any connections via SSH.

Put in your Bitbucket username and password. This may throw an error if you have not done proxy setup (see below). If you get an error, continue on and then do the section listed below under `Proxy Setup`.

Create the following folder structure using Windows Explorer: `C:\DEV\git` (This can be whatever location you like on the C:\ drive.  LAN drives cause performance issues.)

Go to Tools, Options and under General, set your project directory to be `C:\DEV\git` or whatever custom location you have decided on.

Click the checkbox labeled 'Push to default/origin remote when commiting'.

### Bitbucket Browsing

You can use the 'hosted repositories' button to browse and clone from Bitbucket. This is in the left hand pane of SourceTree, on the bottom right. It looks like 3 stacked discs with a gear in front of them. There is some initial setup.

#### Config

You should click the `Edit Accounts` button once the Hosted Repositories window opens. Select your account and click `Edit`. You must set your Preferred Protocol Method to be HTTPS and click the 'Show Private Repositories' button.

Team repositories, like ones owned by `RS_IT`, are only visible here once you 'watch' them in SouceTree. This is done by clicking the 'eye' icon on the repo home page in Bitbucket.

### Setup for using SourceTree via SSH

It's recommended that you have done the basic git and SSH setup before setting up Sourcetree. Sourcetree will pick up configuration details automatically if that's done.

On the initial launch screen, you should see your git username and email address populated for you if you have done the above. If not, enter your full name and OneAmerica email address. Select the 'Allow Sourcetree to modify your global config files' option. Also select the 'automatic line endings' option.

Choose 'No' when prompted about the option for a global .gitgnore file.

If prompted to download/install Mercurial, select "I don't want to use mmercurial".

You must select `Use OpenSSH` because of network restrictions inside OneAmerica.

Put in your Bitbucket username and password. This may throw an error if you have not done proxy setup (see below).

Go to Tools, Options and under General, add your SSH key. Browse to `C:\DEV\git\.ssh` and select `id_rsa`.

Close and reopen Sourcetree. If your SSH key has a passphrase, you will be asked for it.

### Bitbucket Browsing

You can use the 'hosted repositories' button to browse and clone from Bitbucket. There is some initial setup.

#### Config

You should click the `Edit Accounts` button once the Hosted Repositories window opens. Select your account and click `Edit`. You must set your Preferred Protocol Method to be SSH and click the 'Show Private Repositories' button.

Team repositories, like ones owned by `RS_IT`, are only visible here once you 'watch' them in SouceTree. This is done by clicking the 'eye' icon on the repo home page in Bitbucket.

### Proxy Setup

* You should use the default settings for the OneAmerica proxy server. If you have not ever changed your SourceTree settings, or you have not run SourceTree before, this should just work.

* If you have changed your system settings regarding using the proxy server, or you get an error on initil launch of SourceTree when trying to connect to Bitbucket, please check the settings below:
    * Go to `Tools/Options/Network` and select the radio button labeled `Use default operating system settings` and uncheck the box for `Proxy server requires username and password`. You can delete any proxy server usernames/passwords under `Tools/Options/Authentication/Saved Passwords` if you had entered a username/password.

Now you should be able to browse repos, pull code, and commit.

### Removing all Sourcetree Configuration options

See [https://confluence.atlassian.com/display/SOURCETREEKB/Wiping+SourceTree+Preferences](https://confluence.atlassian.com/display/SOURCETREEKB/Wiping+SourceTree+Preferences).
