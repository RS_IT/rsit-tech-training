# NodeJS/NPM Setup

## Mac Setup
For mac, suggested path is [homebrew](http://brew.sh/).
Run the commands on the page above, then:

    brew install node

## NodeJS/NPM setup

* Install [nodejs](http://nodejs.org/).
* Change directories into the folder containing your repo, and then run:
`npm install` to pull down all development software. This is needed for running local webservers, linting, etc.

## Using NPM in a project

Run `npm install` inside a repository to pull down dependencies.
