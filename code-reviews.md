**Table of Contents**  *generated with [DocToc](http://doctoc.herokuapp.com/)*

- [Code Reviews](#markdown-header-code-reviews)
	- [Code Review Pointers](#markdown-header-code-review-pointers)
	- [Minimum Requirements](#markdown-header-minimum-requirements)
	- [Things to look for](#markdown-header-things-to-look-for)

# Code Reviews

Code reviews will be done via pull request.

Eventually, each pull request will automatically trigger a build on the CI server.

As the first step in a PR, pull down that branch and run the code to ensure it passes linting.
If not, comment in the PR what needs to be fixed and wait for the next series of pushes.

## Before submitting code for review

- Include instructions as to how the reviewer should get any new dependencies or update other code as needed.
- Include specifics as to what should function in a new manner so the reviewer can understand the task scope.

## After submitting code for review

## Code Review Pointers ##

You are advised to read [11 Practices for Peer Code Review](http://smartbear.com/SmartBear/media/pdfs/WP-CC-11-Best-Practices-of-Peer-Code-Review.pdf).  Some excerpts follow.

- Review fewer than 200-400 lines of code at a time.
- Aim for an inspection rate of less than 300-500 lines of code per hour.
- Budget enough time for a proper review but don't review for more than an hour at a time.
- When you request changes, make sure they are carried out before passing the review.
- Make it a point to have a turn-around time of 24 hours when possible. If you need longer, inform the person asking.

See the [Wiki](http://wiki.oneamerica.com/ws/index.php/Code_Review_Guidelines) page on code reviews as well.

## Minimum Requirements ##

There are certain minimum requirements which if not met will trigger an automatic fail:

 - Run a linting test.  If JSHint has any objection at all the review fails.
 - Run any unit tests.  If any unit tests fail the review fails.
 - At least one new unit test should be added as part of the pull request if it is a new feature.  This is an absolute minimum.
 - The branch must be able to merge into develop without conflict.

## Things to look for ##

 - Readability
  - Can you easily tell what the code is doing?
  - Are comments needed?
  - Should variables be renamed?
  - If you have to scroll the screen to see an entire function it probably needs to be broken up.
  - Functions should have a single purpose.
 - Unit tests.
  - It is the reviewer's prerogative to request more than the bare minimum of one.
  - If you're requesting more you should also be specific about what you want to see tested.

## JS specific items
 - Promises
  - `fail` callbacks
  - If a pending UI state (spinner) is shown before requesting a promise, the pending state (spinner) should be removed in an `always` callback, not `then` or `fail`.
