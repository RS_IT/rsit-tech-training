**Table of Contents**  *generated with [DocToc](http://doctoc.herokuapp.com/)*

- [Linting](#markdown-header-linting)

# Linting

Linting is the practice of automatically parsing code to adhere to style guides and convention

## When to lint

Most repositories will have a grunt/gulp task called 'lint'. It's a good idea to install a pre-commit hook for git that runs this task before every commit, so you never put bad code in place.

Ex - see [https://bitbucket.org/ebus/front-end-scaffolding/src/develop/git/hooks/pre-commit](https://bitbucket.org/ebus/front-end-scaffolding/src/develop/git/hooks/pre-commit).

It is acceptable and expected that when you find code that doesn't meet the standards, you fix it alongside your normal work.
