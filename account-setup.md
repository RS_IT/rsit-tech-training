# Set Up Bitbucket Account

1.  Go to [Bitbucket](https://bitbucket.org), sign up for an account.  
    * It is important to use your OneAmerica email address for this account.
    * Your username should be similar to OA_Username, OAUsername, etc.  

2.  You will receive an email from Bitbucket.  Open it and click “Confirm This Email” in the body.

3.  On the screen that shows afterward with the text 'Thanks for joining Butbucket! - Now Tell us a little about yourself'
    * Fill in the top 3 fields. Location and website are not necessary. 
    * Make sure you use your real name or whatever you go by at work.  This is so the administrator can find you to be put in the RS_IT team.
    
4.  A prompt to create your first repository will be displayed.  This is not necessary but you can if you wish. To move on with the process click 'No thanks.'

# Joining the RSIT Team in Bitbucket

1.  Your screen should now show an option to create or import a repository.
    At this point please send your Bitbucket username and a request to be added to the RSIT team to Shaun Shupert or Ryan McBride.
    
2.  Wait to receive your email confirmation.

3.  When you receive confirmation that you have been added to the RS_IT team, click the Teams dropdown at the top of the Bitbucket page.
    You should see RS_IT as an option. If not contact Shaun or Ryan.
    This RSIT link takes you to see the repositories for the team.  
    (If you want to play around, there is a test project called Test_Project_4DEVs that you can search for and use as you please.)
    
