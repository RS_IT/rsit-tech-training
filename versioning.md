**Table of Contents**  *generated with [DocToc](http://doctoc.herokuapp.com/)*
test
- [Versioning](#markdown-header-versioning)

# Versioning

There are 2 branches that will deal with versions - `develop` and `master`.

- Versioning is a slight modification of [semantic versioning](http://semver.org/).

- Versions are defined in `package.json` that should be present in each app as MAJOR.MINOR.PATCH. This file has meaning beyond our versioning, see [https://www.npmjs.org/doc/package.json.html](https://www.npmjs.org/doc/package.json.html).

- Major version will stay on 0 until a public facing site is released.

- When a change it pushed to the testing environment, the PATCH number should be incremented and the commit changing that tagged as MAJOR.MINOR.PATCH.

    - Ex:`develop` is at 0.1.0. A changed is made and the code should be pushed.

	- After the last code change, a commit with id of abc123 is made that changed `package.json` to 0.1.1.

	- That commit id is then tagged in git as 0.1.1.

- The tag is pushed to BitBucket and the code is released to ST/FT.

- If it doesn't pass testing, then a new tag should be created and that tag pushed again.

- When a release happens, `develop` should jump to the next MINOR version, and reset PATCH to 0. This version should be tagged in git as MAJOR.MINOR.PATCH with PATCH = 0.

    - Ex: For the end of the first release , we will tag a version of `develop` as 1.0.0.

	- Work continues in `develop` and as changes are made that will go out as bugfixes, those changes are tagged as 1.0.1, 1.0.2, 1.0.3......1.0.999 or higher.

	- `master` receives those changes as appropriate.

- Next change in develop that goes out becomes 1.1.1 and the process repeats.
