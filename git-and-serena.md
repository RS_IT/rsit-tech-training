# Git Workflows and mapping to Serena

Serena: import project
Git   : clone

Serena: checkout
Git   : no command, your local working copy always has all of the files

Serena: get all
Git   : checkout

## Labels in Serena vs tags in git

Serena can attach a label individually to files and then all files with that particular label can be pulled.
Git uses tags, which are a pointer to a specific commit in the repository. So when a tag is applied, EVERYTHING that exists in the repository as of that commit can be gotten using that tag.

`git checkout TAG_HERE` would give you a local working copy with all files in the repository as of that commit.
